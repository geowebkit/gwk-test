export class Basic {
    public constructor(private name: string = "name", protected type: string = "type", ) {
        this.name = name;
        this.type = type;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    protected getType(): string {
        return this.type;
    }

    protected setType(type: string): void {
        this.type = type;
    }

}